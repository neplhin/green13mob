import React, {Component} from "react";
import {Button, Col, Container, Dropdown, DropdownButton, Row} from "react-bootstrap";
import './style.css';
import arrow from "../../images/arrow.png"

class Dodatki extends Component {
    constructor(props) {
        super(props);
        this.state = {activeIndex: null};

        console.log("=======================================")
        console.log(props)
        console.log("=======================================")
    }

    render() {
        return (
            <div>
                <Container >
                    <Row>
                        <Dodatok data={this.props.data.data[0]} show={true}/>
                        <Dodatok data={this.props.data.data[1]} show={false}/>
                        <Dodatok data={this.props.data.data[2]} show={false}/>
                        <Dodatok data={this.props.data.data[3]} show={false}/>
                        <Dodatok data={this.props.data.data[4]} show={false}/>
                        <Dodatok data={this.props.data.data[5]} show={false}/>
                    </Row>
                </Container>
            </div>
        );
    }
}

class Dodatok extends Component {
    constructor(props) {
        super(props);

        console.log(props)
        this.state = {
            show: false,
            cssClass: "none dot-block",
            imgClass: "dodatki-btn-img-none"
        };

        this.handleClickBtn = () => {
            if (this.state.show === false) {
                this.setState({show: true, cssClass: "display dot-block",  imgClass: "dodatki-btn-img" })
            } else {
                this.setState({show: false, cssClass: "none dot-block", imgClass: "dodatki-btn-img-none"})
            }
        };

    }

    componentWillMount() {
        if (this.props.show === true) {
            this.setState({show: true, cssClass: "display dot-block", imgClass: "dodatki-btn-img"})
        } else {
            this.setState({show: false, cssClass: "none dot-block", imgClass: "dodatki-btn-img-none"})
        }

    }

    render() {
        const elements = this.props.data.info.map((item) =>
            <li key={item.id}>
                <span className="dot-name">{item.name}</span><span className="dot-price">{item.price}</span>
            </li>
        );

        return (
            <Col style={{paddingLeft: "unset", paddingRight: "17px", paddingBottom: "16px"}}>
                <Button variant="dark" className="dot-btn" onClick={this.handleClickBtn}>{this.props.data.name} <img
                    className={this.state.imgClass}  src={arrow} /></Button>
                <div className={this.state.cssClass}>
                    <ul className="ul-cust">
                        {elements}
                    </ul>
                </div>
            </Col>
        )
    }

}

export default Dodatki;