import React, {Component} from 'react';
import {Button, ButtonToolbar, Card, Col, Container, Modal, Row} from "react-bootstrap";
import img from "../../images/burger_ameal 2.png"
import './style.css';

class MenuItem extends Component {
    constructor(props) {
        super(props);
        console.log(props)

        this.state = {modalShow: false};
    }

    render() {
        let modalClose = () => this.setState({modalShow: false});
        /*<Card.Img className="card-img-cust" variant="top" src={img}
                                  onClick={() => this.setState({modalShow: true})}/>*/
        return (

            <Card style={{width: '291px', height: "548px", border: "none"}}>
                <a href={this.props.data.obj.path_USDZ}  >
                    <Card.Img className="card-img-cust"  variant="top" src={this.props.data.img} />
                </a>

                {/* <iframe width="100%" height="300" frameBorder="0"
                        src={this.props.data.obj.path_USDZ}>
                </iframe> */}

                <Card.Body style={{paddingLeft: "unset", textAlign: "left"}}>
                    <Card.Title className="title grey">{this.props.data.name}</Card.Title>

                    <Card.Text className="text grey">
                        {this.props.data.text}
                    </Card.Text>
                    <Row>
                        <div style={{    width: "214px"}}>
                            <Card.Text className="green text-energy">
                                {this.props.data.info}
                            </Card.Text>
                        </div>
                        <div>
                            <Card.Text className=" text-s" style={{textAlign: "right", marginBottom: "unset"}}>
                                {this.props.data.weight}
                            </Card.Text>
                            <Card.Text className="green text-s card-price" style={{textAlign: "right"}}>
                                {this.props.data.price}
                            </Card.Text>
                        </div>
                    </Row>
                </Card.Body>

                <MyVerticallyCenteredModal
                    show={this.state.modalShow}
                    onHide={modalClose}
                />
            </Card>

        )
            ;
    }
}

class MyVerticallyCenteredModal extends React.Component {
    render() {
        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered

            >
                <Modal.Header closeButton className="">

                </Modal.Header>
                <Modal.Body className="bg" style={{textAlign: "center"}}>

                </Modal.Body>

            </Modal>
        );
    }
}

/*
class App extends React.Component {
    constructor(...args) {
        super(...args);

        this.state = {modalShow: false};
    }

    render() {
        let modalClose = () => this.setState({modalShow: false});

        return (
            <ButtonToolbar>
                <Button
                    variant="primary"
                    onClick={() => this.setState({modalShow: true})}
                >
                    Launch vertically centered modal
                </Button>

                <MyVerticallyCenteredModal
                    show={this.state.modalShow}
                    onHide={modalClose}
                />
            </ButtonToolbar>
        );
    }
}
*/
export default MenuItem;
