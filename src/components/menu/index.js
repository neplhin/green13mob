import React, {Component} from 'react';
import '../style.css';
import ar from '../../images/ar_logo_bg.png'
import Slider from "react-slick";
import {Button, Col, Container, Row} from "react-bootstrap";
import MenuItem from "./item";
import Dodatki from './dodatki.js'


class Menu extends Component {
    constructor(props) {
        super(props);
        console.log(this.props.menu[0])
    }

    render() {
        const settings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 3000,


            centerMode: true,
            variableWidth: true,
            focusOnSelect: true
        };
        return (
            <div id="menu">
                <Container>
                    <Row>
                        <Col className="menu-h1-block">
                            <h1 className="menu-h1">Дослiджуйте наше меню у доповненiй реальностi</h1>
                            <span className="menu-header-span">Як це працює?</span>
                            <img className="menu-header-img" src={ar} alt="AR view"/>
                        </Col>
                    </Row>
                </Container>
                <div className="sliderTitle">{this.props.menu[0].title}</div>
                <Slider {...settings} >
                    <MenuItem data={this.props.menu[0].data[0]}/>
                    <MenuItem data={this.props.menu[0].data[1]}/>
                </Slider>
                <div className="sliderTitle" >{this.props.menu[1].title}</div>
                <Slider {...settings} >
                    <MenuItem data={this.props.menu[1].data[0]}/>
                    <MenuItem data={this.props.menu[1].data[1]}/>
                </Slider>
                <div className="sliderTitle" >{this.props.menu[2].title}</div>
                <Slider {...settings} >
                    <MenuItem data={this.props.menu[2].data[0]}/>
                    <MenuItem data={this.props.menu[2].data[1]}/>
                    <MenuItem data={this.props.menu[2].data[2]}/>
                    <MenuItem data={this.props.menu[2].data[3]}/>
                    <MenuItem data={this.props.menu[2].data[4]}/>
                </Slider>
                <div className="sliderTitle">{this.props.menu[3].title}</div>
                <Slider {...settings} >
                    <MenuItem data={this.props.menu[3].data[0]}/>
                    <MenuItem data={this.props.menu[3].data[1]}/>
                </Slider>
                <div className="sliderTitle" style={{marginBottom:"50px"}}>{this.props.menu[4].title}</div>

                    <MenuItem data={this.props.menu[4].data[0]}/>

                <Dodatki data={this.props.menu[5]}/>
            </div>
        )
    }
}

export default Menu;