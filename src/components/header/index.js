import React, {Component} from 'react';
import '../style.css';

import bg from '../../images/header-bg.png'
import call from '../../images/call_ico.svg'
import menu from '../../images/menu_iocn.svg'
import glovo from '../../images/glovo_mob.svg'
import {Button, Col, Container, Row} from "react-bootstrap";
import AnchorLink from 'react-anchor-link-smooth-scroll'

class Header extends Component {
    render() {
        return (
            <div>
                <img src={bg} alt="Green 13" className="header-img"/>

                <Container className="header-obrez-block">
                    <Row>
                        <Col className="header-col">
                            <hr className="header-hr"/>
                            <h1 className="header-h1">Green 13</h1>
                            <span className="header-span-1">Вегетарианська кухня</span>
                            <span className="header-span-2" style={{marginTop: "16px"}}>Бессарабська площа, 2 "Бессарабський ринок "</span>
                            <span className="header-span-2">10:00 - 22:00</span>
                            <Button variant="success" className="header-btn-1">098 294 0231<img
                                className="header-btn-icon-1" src={call} alt="call"/></Button>
                            <AnchorLink href="#menu">
                                <Button variant="secondary" className="header-btn-2">Меню<img
                                    className="header-btn-icon-2" src={menu} alt="call"/></Button>
                            </AnchorLink>
                            <a href="https://glovoapp.com/uk/kie/store/green-13/">
                                <Button variant="warning" className="header-btn-3">Замовити<img
                                    className="header-btn-icon-3" src={glovo} alt="call"/></Button>
                            </a>
                        </Col>
                    </Row>
                </Container>

            </div>
        )
    }
}

export default Header;