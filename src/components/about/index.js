import React, {Component} from 'react';
import '../style.css';

import logo from '../../images/logo_mob.svg'

import {Col, Container, Row} from "react-bootstrap";

class About extends Component {
    render() {
        return (
            <div className="about-block">
                <Container>
                    <Row>
                        <Col className="about-col">
                            <div className="about-h1-block">
                                <h1 className="about-h1">Про нас</h1>
                                <img className="about-img" src={logo} alt=""/>
                            </div>
                            <span className="about-span">
                                Green 13 Cafe Vegan Kitchen
ми стали піонерами Української веган-стріт-культури та працюємо для Вас щодня вже третій рік
                            </span>
                        </Col>
                    </Row>
                </Container>

            </div>
        )
    }
}

export default About;