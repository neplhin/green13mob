import React, {Component} from 'react';
import '../style.css';
import icon from '../../images/logo_map.svg'
import {Map, GoogleApiWrapper, Marker} from 'google-maps-react';

const mapStyles = {

    height: '390px'
};

class BlockMap extends Component {

    render() {
        return (
            <div>
                <h1 className="map-h1">Як нас знайти?</h1>
                <Map
                    google={this.props.google}
                    zoom={14}
                    style={mapStyles}
                    initialCenter={{
                        lat: 50.442372,
                        lng: 30.521609
                    }}
                >
                    <Marker
                        position={{lat: 50.442402, lng: 30.521748}}
                        icon={icon}/>
                    <Marker
                        position={{lat: 50.444997, lng: 30.505643}}
                        icon={icon}/>

                </Map>

            </div>
        );
    }


}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyCZo2xjHHDvLyZIuocbb1ohVXw6zv75Xzc'
})(BlockMap);
