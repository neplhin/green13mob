import React, {Component} from 'react';
import '../style.css';

import call from '../../images/call_ico.svg'
import glovo from '../../images/glovo_mob.svg'
import fb from '../../images/fb_5.svg'
import inst from '../../images/Combined Shape.svg'
import designed from '../../images/logo-ameal.svg'

import {Button, Col, Container, Row} from "react-bootstrap";

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <Container>
                    <Row>
                        <Col className="footer-col">
                            <h1 className="footer-h1">Green 13</h1>
                            <div>
                                <a href="https://www.facebook.com/Green13Cafe/">
                                    <img width={32} style={{marginRight: "15px"}} src={fb} alt="Facebook"/>
                                </a>
                                <a href="https://www.instagram.com/Green13Cafe/">
                                    <img width={32} src={inst} alt="Instagram"/>
                                </a>
                            </div>
                            <span className="footer-span"
                                  style={{marginBottom: "15px", marginTop: "15px", display: "block"}}>Бессарабська площа, 2<br/>
"Бессарабський ринок "</span>
                            <span className="footer-span" style={{display: "block"}}>10:00 - 22:00</span>
                            <Button variant="success" className="header-btn-1">098 294 0231<img
                                className="header-btn-icon-1" src={call} alt="call"/></Button>
                            <a href="https://glovoapp.com/uk/kie/store/green-13/">
                                <Button variant="warning" className="header-btn-3">Замовити<img
                                    className="header-btn-icon-3" src={glovo} alt="call"/></Button>
                            </a>
                        </Col>
                    </Row>
                    <Row style={{marginTop: "60px", paddingBottom: "35px"}}>
                        <Col>
                            <span className="footer-privacy">Privacy policy</span>
                            <span className="footer-copyright">Ameal 2019 ©</span>
                        </Col>
                        <Col>
                            <a href="https://ameal.me/">
                                <img className="footer-logo-img" src={designed} alt="Designed by Ameal"/>
                            </a>
                        </Col>
                    </Row>
                </Container>

            </div>
        )
    }
}

export default Footer;