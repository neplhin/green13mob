import React, {Component} from 'react';
import './App.css';
import Header from './components/header'
import Map from './components/map'
import About from './components/about'
import Menu from './components/menu'
import Footer from './components/footer'
import data from "./menu"


class App extends Component {
    render() {
        return (
            <div className="App">
                <WindowDimensions/>
                <Header/>
                <Map/>
                <About/>
                <Menu menu={data}/>
                <Footer/>
            </div>
        );
    }
}

class WindowDimensions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            "width": window.innerWidth,
            "height": window.innerHeight,
        };
    }

    render() {
        return(<div></div>);
    }

    updateDimensions() {
        console.log("resize")
        this.setState({"width": window.innerWidth, "height": window.innerHeight});
        if (this.state.width > 800) {
            window.location ='http://a3boots-wordpress-6.tw1.ru/'
        }
    }


    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions.bind(this));
        if (this.state.width > 800) {
            console.log("did mount")
            window.location ='http://a3boots-wordpress-6.tw1.ru/'
        }
    }

    componentWillUnmount() {
        console.log(window.innerWidth);
        console.log(window.innerHeight);
    }
};

export default App;
